﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ComponentModel;

public static class HSExtensions {}

public static class ObjectExtension {	
	public static T DebugLog<T>(this T o,string message = ""){
		Debug.Log(message+ " " + o);
		return o;
	}
} 

public static class GameObjectExtensions {
	public static void Destroy(this GameObject go) {
		Destroy(go);
	}

	public static T AddComponent<T>(this GameObject go, T toAdd) where T : UnityEngine.Component
	{
		return go.AddComponent<T>().GetCopyOf(toAdd);
	}

	public static T GetCopyOf<T>(this UnityEngine.Component comp, T other) where T : UnityEngine.Component
	{
		var type = comp.GetType();
		if (type != other.GetType()) return null; // type mis-match
		const BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic |
			BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
		var pinfos = type.GetProperties(flags);
		foreach (var pinfo in pinfos.Where(pinfo => pinfo.CanWrite))
		{
			try
			{
				pinfo.SetValue(comp, pinfo.GetValue(other, null), null);
			}
			catch { } // In case of NotImplementedException being thrown. For some reason specifying that exception didn't seem to catch it, so I didn't catch anything specific.
		}
		var finfos = type.GetFields(flags);
		foreach (var finfo in finfos)
			finfo.SetValue(comp, finfo.GetValue(other));

		return comp as T;
	}
}

public static class ActionExtensions {
	public static void Fire(this Action action) {
		if (action != null) action ();
	}

	public static void Fire<T>(this Action<T> action, T t) {
		if (action != null) action (t);
	}

	public static void Fire<T,U>(this Action<T,U> action, T t, U u) {
		if (action != null) action (t, u);
	}

	public static void Fire<T,U,V>(this Action<T,U,V> action, T t, U u, V v) {
		if (action != null) action (t, u, v);
	}

	public static void Fire<T,U,V,W>(this Action<T,U,V,W> action, T t, U u, V v, W w) {
		if (action != null) action (t, u, v , w);
	}
}

public static class ListExtensions {
	public static List<T> WithItems<T>(this List<T> list, params T[] items) {
		list.AddRange(items);
		return list;
	}

	public static void AddUnique<T>(this List<T> list, T item) {
		if(!list.Contains(item))
			list.Add(item);
	}

	public static void RemoveExisting<T>(this List<T> list, T item) {
		if(list.Contains(item)) {
			list.Remove(item);
		}
	}

	public static void RemoveExistings<T>(this List<T> list, params T[] items) {
		foreach(var item in items)
			if(list.Contains(item)) {
				list.Remove(item);
			}
	}

	public static bool HasAny<T>(this List<T> list, params T[] items) {
		foreach(var item in items) {
			if(list.Contains(item)) {
				return true;
			}
		}
		return false;
	}

	public static bool HasAll<T>(this List<T> list, params T[] items) {
		foreach(var item in items) {
			if(!list.Contains(item)) {
				return false;
			}
		}
		return true;
	}

	public static bool HasItems<T>(this List<T> list) {
		return list != null && list.Count > 0;
	}

	public static List<T> SwapObjectsAtIndexes<T>(this List<T> list, int x, int y) {
		if(list.Count > Mathf.Max(x, y)) {
			var temp = list[x];
			list[x] = list[y];
			list[y] = temp;
		}
		else {
			Debug.LogWarningFormat("Could not swap indexes: [{0}, {1}]. List count too low: [{2}].", x, y, list.Count);
		}
		return list;
	}

	public static void QuickSort<T>(this List<T> list,Comparison<T> comparison){

		QuickSort (list, comparison, 0, list.Count-1);
	}

	public static void QuickSort<T>(this List<T> list,Comparer<T> comparer){

		QuickSort (list, comparer, 0, list.Count-1);
	}

	public static void QuickSort<T>(this List<T> list,Comparison<T> comparison, int leftIndex, int rightIndex)
	{
		int i = leftIndex, j = rightIndex;
		var pivot = list[(leftIndex + rightIndex) / 2];

		while (i <= j)
		{
			while (comparison(list [i],pivot) < 0) {
				i++;
			}

			while (comparison(list [j],pivot) > 0) {
				j--;
			}

			if (i <= j)
			{
				// Swap
				var tmp = list[i];
				list[i] = list[j];
				list[j] = tmp;
				i++;
				j--;
			}
		}
		// Recursive calls
		if (leftIndex < j) {
			QuickSort(list,comparison, leftIndex, j);
		}

		if (i < rightIndex){
			QuickSort(list,comparison, i, rightIndex);
		}
	}

	public static void QuickSort<T>(this List<T> elements,Comparer<T> comparer, int left, int right)
	{
		int i = left, j = right;
		var pivot = elements[(left + right) / 2];

		while (i <= j)
		{
			while (comparer.Compare(elements [i],pivot) < 0) {
				i++;
			}

			while (comparer.Compare(elements [j],pivot) > 0) {
				j--;
			}

			if (i <= j)
			{
				// Swap
				var tmp = elements[i];
				elements[i] = elements[j];
				elements[j] = tmp;
				i++;
				j--;
			}
		}
		// Recursive calls
		if (left < j) {
			QuickSort(elements,comparer, left, j);
		}

		if (i < right){
			QuickSort(elements,comparer, i, right);
		}
	}
}

public static class ArrayExtensions {
	public static void ForEach<T>(this T[] array, Action<T> action) {
		Array.ForEach(array, action);
	}

	public static T[] SwapObjectsAtIndexes<T>(this T[] array, int x, int y) {
		if(array.Length > Mathf.Max(x, y)) {
			var temp = array[x];
			array[x] = array[y];
			array[y] = temp;
		}
		else {
			Debug.LogWarningFormat("Could not swap indexes: [{0}, {1}]. List count too low: [{2}].", x, y, array.Length);
		}
		return array;
	}

	public static void QuickSort<T>(this T[] array,Comparison<T> comparison){

		QuickSort (array, comparison, 0, array.Length-1);
	}

	public static void QuickSort<T>(this T[] array,Comparer<T> comparer){

		QuickSort (array, comparer, 0, array.Length-1);
	}

	public static void QuickSort<T>(this T[] array,Comparison<T> comparison, int leftIndex, int rightIndex)
	{
		int i = leftIndex, j = rightIndex;
		var pivot = array[(leftIndex + rightIndex) / 2];

		while (i <= j)
		{
			while (comparison(array [i],pivot) < 0) {
				i++;
			}

			while (comparison(array [j],pivot) > 0) {
				j--;
			}

			if (i <= j)
			{
				// Swap
				var tmp = array[i];
				array[i] = array[j];
				array[j] = tmp;
				i++;
				j--;
			}
		}
		// Recursive calls
		if (leftIndex < j) {
			QuickSort(array,comparison, leftIndex, j);
		}

		if (i < rightIndex){
			QuickSort(array,comparison, i, rightIndex);
		}
	}

	public static void QuickSort<T>(this T[] array,Comparer<T> comparer, int leftIndex, int rightIndex)
	{
		int i = leftIndex, j = rightIndex;
		var pivot = array[(leftIndex + rightIndex) / 2];

		while (i <= j)
		{
			while (comparer.Compare(array [i],pivot) < 0) {
				i++;
			}

			while (comparer.Compare(array [j],pivot) > 0) {
				j--;
			}

			if (i <= j)
			{
				// Swap
				var tmp = array[i];
				array[i] = array[j];
				array[j] = tmp;
				i++;
				j--;
			}
		}
		// Recursive calls
		if (leftIndex < j) {
			QuickSort(array,comparer, leftIndex, j);
		}

		if (i < rightIndex){
			QuickSort(array,comparer, i, rightIndex);
		}
	}
}

public static class TransformExtensions {
	
	public static T[] GetComponentsInChildren<T>
	(this GameObject go, bool includeInactive, bool skipSelf = false)
		where T : UnityEngine.Component
	{
		var comps = go.GetComponentsInChildren<T>(includeInactive);
		if (!skipSelf)
			return comps;

		return comps.Where(comp => comp.gameObject.GetInstanceID() != go.GetInstanceID()).ToArray();
	}

	public static IEnumerator CoScaleObject(this Transform objTransform,Vector3 startScale,Vector3 endScale, float time, float delay)
	{
		yield return new WaitForSeconds(delay);
		var rate = 1.0f / time;
		var i = 0f;
	
		while (i < 1.0f)
		{
			i += Time.deltaTime * rate;
			objTransform.localScale = Vector3.Lerp(startScale, endScale, i);
			yield return 0;
		}
	}

	public static IEnumerator CoMoveObjectLocal(this Transform objTransform,Vector3 startPos,Vector3 endPos, float time, float delay)
	{
		yield return new WaitForSeconds(delay);
		var rate = 1.0f / time;
		var i = 0f;

		while (i < 1.0f)
		{
			i += Time.deltaTime * rate;
			objTransform.localPosition = Vector3.Lerp(startPos, endPos, i);
			yield return 0;
		}
	}

	public static IEnumerator CoMoveObjectWolrd(this Transform objTransform,Vector3 startPos,Vector3 endPos, float time, float delay)
	{
		yield return new WaitForSeconds(delay);
		var rate = 1.0f / time;
		var i = 0f;

		while (i < 1.0f)
		{
			i += Time.deltaTime * rate;
			objTransform.position = Vector3.Lerp(startPos, endPos, i);
			yield return 0;
		}
	}

	public static void SetX(this Transform t, float newX) {
		t.position = new Vector3(newX, t.position.y, t.position.z);
	}

	public static void SetY(this Transform t, float newY) {
		t.position = new Vector3(t.position.x, newY, t.position.z);
	}

	public static void SetZ(this Transform t, float newZ) {
		t.position = new Vector3(t.position.x, t.position.y, newZ);
	}

	public static float GetX(this Transform t) {
		return t.position.x;
	}

	public static float GetY(this Transform t) {
		return t.position.y;
	}

	public static float GetZ(this Transform t) {
		return t.position.z;
	}

	public static void IncreaseX(this Transform t, float amount) {
		t.SetX(t.GetX() + amount);
	}

	public static void IncreaseY(this Transform t, float amount) {
		t.SetY(t.GetY() + amount);
	}

	public static void IncreaseZ(this Transform t, float amount) {
		t.SetZ(t.GetZ() + amount);
	}
}

public static class RectTransformExtensions {
	public static void AnchorToCorners(this RectTransform transform) {
		if (transform == null)
			throw new ArgumentNullException("transform");
		if (transform.parent == null)
			return;
		var parent = transform.parent.GetComponent<RectTransform>();
		var newAnchorsMin = new Vector2(transform.anchorMin.x + transform.offsetMin.x / parent.rect.width, transform.anchorMin.y + transform.offsetMin.y / parent.rect.height);
		var newAnchorsMax = new Vector2(transform.anchorMax.x + transform.offsetMax.x / parent.rect.width, transform.anchorMax.y + transform.offsetMax.y / parent.rect.height);
		transform.anchorMin = newAnchorsMin;
		transform.anchorMax = newAnchorsMax;
		transform.offsetMin = transform.offsetMax = new Vector2(0, 0);
	}

	public static void SetPivotAndAnchors(this RectTransform trans, Vector2 aVec) {
		trans.pivot = aVec;
		trans.anchorMin = aVec;
		trans.anchorMax = aVec;
	}

	public static Vector2 GetSize(this RectTransform trans) {
		return trans.rect.size;
	}

	public static float GetWidth(this RectTransform trans) {
		return trans.rect.width;
	}

	public static float GetHeight(this RectTransform trans) {
		return trans.rect.height;
	}

	public static void SetSize(this RectTransform trans, Vector2 newSize) {
		var oldSize = trans.rect.size;
		var deltaSize = newSize - oldSize;
		trans.offsetMin = trans.offsetMin - new Vector2(deltaSize.x * trans.pivot.x, deltaSize.y * trans.pivot.y);
		trans.offsetMax = trans.offsetMax + new Vector2(deltaSize.x * (1f - trans.pivot.x), deltaSize.y * (1f - trans.pivot.y));
	}

	public static void SetWidth(this RectTransform trans, float newSize) {
		SetSize(trans, new Vector2(newSize, trans.rect.size.y));
	}

	public static void SetHeight(this RectTransform trans, float newSize) {
		SetSize(trans, new Vector2(trans.rect.size.x, newSize));
	}

	public static void SetBottomLeftPosition(this RectTransform trans, Vector2 newPos) {
		trans.localPosition = new Vector3(newPos.x + (trans.pivot.x * trans.rect.width), newPos.y + (trans.pivot.y * trans.rect.height), trans.localPosition.z);
	}

	public static void SetTopLeftPosition(this RectTransform trans, Vector2 newPos) {
		trans.localPosition = new Vector3(newPos.x + (trans.pivot.x * trans.rect.width), newPos.y - ((1f - trans.pivot.y) * trans.rect.height), trans.localPosition.z);
	}

	public static void SetBottomRightPosition(this RectTransform trans, Vector2 newPos) {
		trans.localPosition = new Vector3(newPos.x - ((1f - trans.pivot.x) * trans.rect.width), newPos.y + (trans.pivot.y * trans.rect.height), trans.localPosition.z);
	}

	public static void SetRightTopPosition(this RectTransform trans, Vector2 newPos) {
		trans.localPosition = new Vector3(newPos.x - ((1f - trans.pivot.x) * trans.rect.width), newPos.y - ((1f - trans.pivot.y) * trans.rect.height), trans.localPosition.z);
	}

}

public static class ComponentExtensions {
	public static T GetOrAddComponent<T> (this UnityEngine.Component child) where T: UnityEngine.Component {
		return child.GetComponent<T>() ?? child.gameObject.AddComponent<T>();
	}

	public static void Destroy(this UnityEngine.Component com) {
		Destroy(com);
	}
}

public static class AnimationExtensions {
	public static void SetSpeed(this Animation anim, float newSpeed) {
		anim[anim.clip.name].speed = newSpeed; 
	}

	public static void SetTime(this Animation anim, float newTime) {
		anim[anim.clip.name].time = newTime; 
	}

}

public static class ColorExtensions {
	public static Color SetAlpha(this Color color, float alpha) {
		return new Color(color.r, color.g, color.b, alpha);
	}

	public static Color IncreaseAlpha(this Color color, float amount) {
		return new Color(color.r, color.g, color.b, color.a + amount);
	}

	public static Color DecreaseAlpha(this Color color, float amount) {
		return new Color(color.r, color.g, color.b, color.a - amount);
	}
}

public static class RendererExtensions {
	public static bool IsVisibleFrom(this Renderer renderer, Camera camera) {
		var planes = GeometryUtility.CalculateFrustumPlanes(camera);
		return GeometryUtility.TestPlanesAABB(planes, renderer.bounds);
	}
}

public static class EnumExtensions {
	public static int? EnumToInt<T>(this T _enum) where T: IConvertible, IComparable, IFormattable {
		if(Enum.IsDefined(typeof(T), _enum)) {
			var intEnum = (int)Convert.ChangeType(_enum, typeof(int));
			return intEnum;
		}
		else {
			return null;
		}
	}
}

public static class StringExtensions {
	public static bool emptyOrNull(this string text) {
		return string.IsNullOrEmpty(text);
	}

	public static bool TryParse<T>(this string valueToParse, out T returnValue) {
		returnValue = default(T);    
		if (Enum.IsDefined(typeof(T), valueToParse)) {
			System.ComponentModel.TypeConverter converter = System.ComponentModel.TypeDescriptor.GetConverter(typeof(T));
			returnValue = (T)converter.ConvertFromString(valueToParse);
			return true;
		}
		return false;
	}

	public static int? TryIntParse(this string valueToParse) {
		int val;
		if (int.TryParse(valueToParse, out val)) 
			return val; 
		else 
			return null;
	}

	public static Int64? TryLongParse(this string valueToParse) {
		Int64 val;
		if (Int64.TryParse(valueToParse, out val)) 
			return val; 
		else 
			return null;
	}

	public static float? TryFloatParse(this string valueToParse) {
		float val;
		if(float.TryParse(valueToParse, out val)) 
			return val; 
		else 
			return null;
	}

	public static double? TryDoubleParse(this string valueToParse) {
		double val;
		if(double.TryParse(valueToParse, out val)) 
			return val; 
		else 
			return null;
	}

	public static DateTime? TryDateTimeParse(this string valueToParse) {
		DateTime val;
		if(DateTime.TryParse(valueToParse, out val))
			return val; 
		else 
			return null;
	}

	public static bool TryParse(this string valueToParse, out double returnValue) {
		return double.TryParse(valueToParse, out returnValue);
	}

	public static bool TryParse(this string valueToParse, out float returnValue) {
		return float.TryParse(valueToParse, out returnValue);
	}

	public static bool TryParse(this string valueToParse, out int returnValue) {
		return int.TryParse(valueToParse, out returnValue);
	}

	public static int IntParse(this string val) {
		return val.TryIntParse () ?? 0; }

	public static float FloatParse(this string val) {
		return val.TryFloatParse () ?? 0;
	}

	public static double DoubleParse(this string val) {
		return val.TryDoubleParse () ?? 0;
	}

	public static T EnumParse<T>(this string valueToParse) where T: struct  {
		if (Enum.IsDefined (typeof(T), valueToParse)) {
			return (T)Enum.Parse (typeof(T), valueToParse);
		}
		throw new UnityException ("The passed in string: [" + valueToParse + "] is not an ENUM and you are trying to parse it. Use TryParse instead.");
	}

	public static T NumberParse<T>(this string valueToParse) where T: class {
		var parseResult = valueToParse.TryIntParse() ?? valueToParse.TryFloatParse() ?? valueToParse.TryDoubleParse();

		if (parseResult.HasValue) {
			return parseResult as T;
		}
		throw new UnityException("The passed in string: [" + valueToParse + "] is not an int, float, or even double and you are trying to parse it. Use TryParse instead.");
	}
}

public static class FloatExtensions {
	public static int ToInt(this float obj) {
		return (int)Convert.ChangeType(obj, typeof(int));
	}
}

public static class DoubleExtensions {
	public static int ToInt(this double obj) {
		return (int)Convert.ChangeType(obj, typeof(int));
	}
}

public static class GenericExtensions {
	public static bool IsBetween<T>(this T actual, T lower, T upper, bool excludeMax = false) where T : IComparable<T> {
		if(!excludeMax)
			return actual.CompareTo(lower) >= 0 && actual.CompareTo(upper) < 0;
		return actual.CompareTo(lower) > 0 && actual.CompareTo(upper) < 0;
	}

	public static bool Has<T>(this T source, params T[] list) {
		if (source == null)
			throw new ArgumentNullException("source");
		return list.Contains(source);
	}

	public static List<T> InList<T>(this T item) {
		return new List<T> { item };
	}

	public static T[] InArray<T>(this T item) {
		return new [] { item };
	}
}
