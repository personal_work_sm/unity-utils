﻿
// Alternative version, with redundant code removed
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(Transform)),CanEditMultipleObjects]
public class CustomTransform:Editor
{
	string valuePrefix = "";
	string valueSuffix = "";
	string valueReplaceString = "";
	int valueRenameBegin = 0;
	int valueDeleteBegin = 0;
	int valueDeleteLenght = 0;
	bool renameOptions = false;

	bool operationsOnTranform = false;
	TransformValues addToPosition = new TransformValues();
	TransformValues multiplyPosition = new TransformValues();
	TransformValues addToRotation = new TransformValues();
	TransformValues multiplyRotation = new TransformValues();
	TransformValues addToScale = new TransformValues();
	TransformValues multiplyScale = new TransformValues();

	SerializedProperty mPos;
	SerializedProperty mRot;
	SerializedProperty mScale;

	void OnEnable ()
	{
		this.mPos = this.serializedObject.FindProperty("m_LocalPosition");
		this.mRot = this.serializedObject.FindProperty("m_LocalRotation");
		this.mScale = this.serializedObject.FindProperty("m_LocalScale");
	}


	public override void OnInspectorGUI()
	{
		
		this.serializedObject.Update();

		EditorGUIUtility.labelWidth = 10;

		DrawPosition();
		DrawRotation();
		DrawScale();

		GUILayout.Space(10);

		EditorGUIUtility.labelWidth = 200;
		operationsOnTranform = GUILayout.Toggle(operationsOnTranform,"Operations on Transform");
		if(operationsOnTranform)
		{
			GUILayout.Space(10);
			Operations(addToPosition,OperationType.Add,ComponentType.Position,"AddToPosition");
			Operations(multiplyPosition,OperationType.Multiply,ComponentType.Position,"MultiplyPosition");
			Operations(addToRotation,OperationType.Add,ComponentType.Rotation,"AddToRotation");
			Operations(multiplyRotation,OperationType.Multiply,ComponentType.Rotation,"MultiplyRotation");
			Operations(addToScale,OperationType.Add,ComponentType.Scale,"AddToScale");
			Operations(multiplyScale,OperationType.Multiply,ComponentType.Scale,"MultiplyScale");
			GUILayout.Space(10);
		}

		if (AssetDatabase.Contains(this.target))
			return;

		EditorGUIUtility.labelWidth = 65;
		renameOptions = GUILayout.Toggle(renameOptions,"Rename Options");
		if(renameOptions)
		{
			GUILayout.Space(10);
			AddPrefix();
			AddSufix();
			ReplaceCharacter();
			DeleteCharacter();
		}

		this.serializedObject.ApplyModifiedProperties();
	}

	#region Rename

	void AddPrefix()
	{
		GUILayout.BeginHorizontal();

		if(GUILayout.Button("Add Prefix",GUILayout.Width(100f)) && valuePrefix != ""){
			foreach(GameObject go in Selection.gameObjects)
			{
				go.name = valuePrefix + go.name;
			}
			valuePrefix = "";
		}

		valuePrefix = EditorGUILayout.TextField("inputString",valuePrefix,GUILayout.MinWidth(50));

		GUILayout.EndHorizontal();
	}

	void AddSufix()
	{
		GUILayout.BeginHorizontal();

		if(GUILayout.Button("Add Suffix",GUILayout.Width(100f)) && valueSuffix != ""){
			foreach(GameObject go in Selection.gameObjects)
			{
				go.name = go.name + valueSuffix;
			}
			valueSuffix = "";
		}

		valueSuffix = EditorGUILayout.TextField("inputString",valueSuffix,GUILayout.MinWidth(50));

		GUILayout.EndHorizontal();
	}

	void ReplaceCharacter()
	{
		GUILayout.BeginHorizontal();

		if(GUILayout.Button("Replace",GUILayout.Width(100f)) && valueReplaceString != ""){
			foreach(GameObject go in Selection.gameObjects)
			{
				var name = go.name.ToCharArray();
				var replace = valueReplaceString.ToCharArray();
				List<char> newName = new List<char>(name);

				for(int i = valueRenameBegin;i< valueReplaceString.Length + valueRenameBegin; i++)
				{
					if(i < newName.Count)
						newName[i] = replace[i - valueRenameBegin];
					else
					{
						newName.Add(replace[i - valueRenameBegin]);
					}

				}
				go.name = new string (newName.ToArray());
			}
			valueReplaceString = "";
			valueRenameBegin = 0;
		}


		valueRenameBegin = EditorGUILayout.IntField("startIndex",valueRenameBegin, GUILayout.ExpandWidth(false));

		valueReplaceString = EditorGUILayout.TextField("inputString",valueReplaceString,GUILayout.MinWidth(50));

		GUILayout.EndHorizontal();
	}

	void DeleteCharacter()
	{
		GUILayout.BeginHorizontal();

		if(GUILayout.Button("Delete",GUILayout.Width(100f)) && valueDeleteLenght != 0){
			foreach(GameObject go in Selection.gameObjects)
			{
				var name = go.name.ToCharArray();
				List<char> newName = new List<char>();

				for(int i = 0;i< name.Length; i++)
				{
					if(i < valueDeleteBegin || i >= valueDeleteBegin + valueDeleteLenght)
						newName.Add(name[i]);
				}
				go.name = new string (newName.ToArray());
			}

			valueDeleteBegin = 0;
			valueDeleteLenght = 0;
		}

		valueDeleteBegin = EditorGUILayout.IntField("startIndex",valueDeleteBegin,GUILayout.MinWidth(50));
		valueDeleteLenght = EditorGUILayout.IntField("lenght",valueDeleteLenght,GUILayout.MinWidth(50));

		GUILayout.EndHorizontal();
	}

	#endregion

	#region Tranform

	void DrawPosition()
	{
		GUILayout.BeginHorizontal();

		if(GUILayout.Button("Position",GUILayout.MinWidth(70f))){
			this.mPos.vector3Value = Vector3.zero;
		}

		GUILayoutOption opt = GUILayout.MinWidth(50f);

		EditorGUILayout.PropertyField(mPos.FindPropertyRelative("x"),opt);
		EditorGUILayout.PropertyField(mPos.FindPropertyRelative("y"),opt);
		EditorGUILayout.PropertyField(mPos.FindPropertyRelative("z"),opt);

		GUILayout.EndHorizontal();
	}

	void DrawScale()
	{
		GUILayout.BeginHorizontal();

		if(GUILayout.Button("Scale",GUILayout.MinWidth(70f))){
			this.mScale.vector3Value = Vector3.one;
		}

		GUILayoutOption opt = GUILayout.MinWidth(50f);

		EditorGUILayout.PropertyField(mScale.FindPropertyRelative("x"),opt);
		EditorGUILayout.PropertyField(mScale.FindPropertyRelative("y"),opt);
		EditorGUILayout.PropertyField(mScale.FindPropertyRelative("z"),opt);

		GUILayout.EndHorizontal();
	}



	enum Axes : int
	{
		None = 0,
		X = 1,
		Y = 2,
		Z = 4,
		All = 7,
	}

	Axes CheckDifference (Transform t, Vector3 original)
	{
		Vector3 next = t.localEulerAngles;

		Axes axes = Axes.None;

		if (Differs(next.x, original.x)) axes |= Axes.X;
		if (Differs(next.y, original.y)) axes |= Axes.Y;
		if (Differs(next.z, original.z)) axes |= Axes.Z;

		return axes;
	}

	Axes CheckDifference (SerializedProperty property)
	{
		Axes axes = Axes.None;

		if (property.hasMultipleDifferentValues)
		{
			Vector3 original = property.quaternionValue.eulerAngles;

			foreach (Object obj in serializedObject.targetObjects)
			{
				axes |= CheckDifference(obj as Transform, original);
				if (axes == Axes.All) break;
			}
		}
		return axes;
	}

	/// <summary>
	/// Draw an editable float field.
	/// </summary>
	/// <param name="hidden">Whether to replace the value with a dash</param>
	/// <param name="greyedOut">Whether the value should be greyed out or not</param>

	static bool FloatField (string name, ref float value, bool hidden, bool greyedOut, GUILayoutOption opt)
	{
		float newValue = value;
		GUI.changed = false;

		if (!hidden)
		{
			if (greyedOut)
			{
				GUI.color = new Color(0.7f, 0.7f, 0.7f);
				newValue = EditorGUILayout.FloatField(name,newValue, opt);
				GUI.color = Color.white;
			}
			else
			{
				newValue = EditorGUILayout.FloatField(name,newValue, opt);
			}
		}
		else if (greyedOut)
		{
			GUI.color = new Color(0.7f, 0.7f, 0.7f);
			float.TryParse(EditorGUILayout.TextField(name,"—", opt), out newValue);
			GUI.color = Color.white;
		}
		else
		{
			float.TryParse(EditorGUILayout.TextField(name,"—", opt), out newValue);
		}

		if (GUI.changed && Differs(newValue, value))
		{
			value = newValue;
			return true;
		}
		return false;
	}

	/// <summary>
	/// Because Mathf.Approximately is too sensitive.
	/// </summary>

	static bool Differs (float a, float b) { return Mathf.Abs(a - b) > 0.0001f; }

	void DrawRotation ()
	{
		GUILayout.BeginHorizontal();
		{
			bool reset = GUILayout.Button("Rotation", GUILayout.MinWidth(70f));

			Vector3 visible = (serializedObject.targetObject as Transform).localEulerAngles;

			visible.x = WrapAngle(visible.x);
			visible.y = WrapAngle(visible.y);
			visible.z = WrapAngle(visible.z);

			Axes changed = CheckDifference(mRot);
			Axes altered = Axes.None;

			GUILayoutOption opt = GUILayout.MinWidth(50f);

			if (FloatField("X", ref visible.x, (changed & Axes.X) != 0, false, opt)) altered |= Axes.X;
			if (FloatField("Y", ref visible.y, (changed & Axes.Y) != 0, false, opt)) altered |= Axes.Y;
			if (FloatField("Z", ref visible.z, (changed & Axes.Z) != 0, false, opt)) altered |= Axes.Z;


			if (reset)
			{
				this.mRot.quaternionValue = Quaternion.identity;
			}
			else if (altered != Axes.None)
			{
				RegisterUndo("Change Rotation", serializedObject.targetObjects);

				foreach (Object obj in serializedObject.targetObjects)
				{
					Transform t = obj as Transform;
					Vector3 v = t.localEulerAngles;

					if ((altered & Axes.X) != 0) v.x = visible.x;
					if ((altered & Axes.Y) != 0) v.y = visible.y;
					if ((altered & Axes.Z) != 0) v.z = visible.z;

					t.localEulerAngles = v;
				}
			}
		}
		GUILayout.EndHorizontal();
	}

	static public float WrapAngle (float angle)
	{
		while (angle > 180f) angle -= 360f;
		while (angle < -180f) angle += 360f;
		return angle;
	}

	static public void RegisterUndo (string name, params Object[] objects)
	{
		if (objects != null && objects.Length > 0)
		{
			UnityEditor.Undo.RecordObjects(objects, name);

			foreach (Object obj in objects)
			{
				if (obj == null) continue;
				EditorUtility.SetDirty(obj);
			}
		}
	}

	void Operations(TransformValues classValues,OperationType op,ComponentType comp,string buttonName)
	{
		GUILayout.BeginHorizontal();

		if(GUILayout.Button(buttonName,GUILayout.Width(100f))){
			foreach(GameObject go in Selection.gameObjects)
			{
				switch(comp)
				{
					case ComponentType.Position:
						var pos = go.transform.localPosition;
						switch(op)
						{
							case OperationType.Add:
							if(classValues.selectedX) pos.x +=  classValues.ValueX;
							if(classValues.selectedY) pos.y +=  classValues.ValueY;
							if(classValues.selectedZ) pos.z +=  classValues.ValueZ;
								break;
							case OperationType.Multiply:
							if(classValues.selectedX) pos.x *=  classValues.ValueX;
							if(classValues.selectedY) pos.y *=  classValues.ValueY;
							if(classValues.selectedZ) pos.z *=  classValues.ValueZ;
								break;
						}
						go.transform.localPosition = pos;
						break;
					case ComponentType.Rotation:
						var rot = go.transform.localRotation.eulerAngles;
						switch(op)
						{
						case OperationType.Add:
							if(classValues.selectedX) rot.x +=  classValues.ValueX;
							if(classValues.selectedY) rot.y +=  classValues.ValueY;
							if(classValues.selectedZ) rot.z +=  classValues.ValueZ;
							break;
						case OperationType.Multiply:
							if(classValues.selectedX) rot.x *=  classValues.ValueX;
							if(classValues.selectedY) rot.y *=  classValues.ValueY;
							if(classValues.selectedZ) rot.z *=  classValues.ValueZ;
							break;
						}
						go.transform.localRotation = Quaternion.Euler(rot);
						break;
					case ComponentType.Scale:
						var scale = go.transform.localScale;
						switch(op)
						{
							case OperationType.Add:
							if(classValues.selectedX) scale.x +=  classValues.ValueX;
							if(classValues.selectedY) scale.y +=  classValues.ValueY;
							if(classValues.selectedZ) scale.z +=  classValues.ValueZ;
							break;
							case OperationType.Multiply:
							if(classValues.selectedX) scale.x *=  classValues.ValueX;
							if(classValues.selectedY) scale.y *=  classValues.ValueY;
							if(classValues.selectedZ) scale.z *=  classValues.ValueZ;
							break;
						}
						go.transform.localScale = scale;
							break;
				}
			}

		}


		classValues.selectedX = GUILayout.Toggle( classValues.selectedX,"X",GUILayout.Width(30));
		EditorGUIUtility.labelWidth = 10;
		if(classValues.selectedX){
			EditorGUIUtility.labelWidth = 40;
			classValues.ValueX = EditorGUILayout.FloatField("Value",classValues.ValueX,GUILayout.MinWidth(30),GUILayout.MaxWidth(150));
		}else
		{
			classValues.ValueX = 0;
		}

		EditorGUIUtility.labelWidth = 10;
		classValues.selectedY = GUILayout.Toggle(classValues.selectedY,"Y",GUILayout.Width(30));
		if(classValues.selectedY){
			EditorGUIUtility.labelWidth = 40;
			classValues.ValueY = EditorGUILayout.FloatField("Value",classValues.ValueY,GUILayout.MinWidth(30),GUILayout.MaxWidth(150));
		}else
		{
			classValues.ValueY = 0;
		}

		EditorGUIUtility.labelWidth = 10;
		classValues.selectedZ = GUILayout.Toggle(classValues.selectedZ,"Z",GUILayout.Width(30));
		if(classValues.selectedZ){
			EditorGUIUtility.labelWidth = 40;
			classValues.ValueZ = EditorGUILayout.FloatField("Value",classValues.ValueZ,GUILayout.MinWidth(30),GUILayout.MaxWidth(150));
		}else
		{
			classValues.ValueZ = 0;
		}

		GUILayout.EndHorizontal();
	}

	#endregion
}
	
public class TransformValues
{
	public bool selectedX = false;
	public bool selectedY = false;
	public bool selectedZ = false;

	public float ValueX = 0;
	public float ValueY = 0;
	public float ValueZ = 0;
}

public enum OperationType{
	Add,
	Multiply,
	Subtract,
	Devide
}

public enum ComponentType{
	Position,
	Rotation,
	Scale,
}
