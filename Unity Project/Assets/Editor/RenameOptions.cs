﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class RenameOptions : EditorWindow {
	
	string valuePrefix = "";
	string valueSuffix = "";
	string valueReplaceString = "";
	int valueRenameBegin = 0;
	int valueDeleteBegin = 0;
	int valueDeleteLenght = 0;

	// Add menu named "My Window" to the Window menu
	[MenuItem ("Tools/Rename Options")]
	static void Init () {
		// Get existing open window or if none, make a new one:
		RenameOptions window = (RenameOptions)EditorWindow.GetWindow (typeof (RenameOptions));
		window.Show();
	}
		

	void OnGUI () {
		EditorGUIUtility.labelWidth = 70;

		GUILayout.BeginHorizontal();

		var centeredStyle = GUI.skin.GetStyle("Label");
		centeredStyle.alignment = TextAnchor.MiddleCenter;
		GUILayout.Label ( "Rename Options", centeredStyle);

		GUILayout.EndHorizontal();

		GUILayout.Space(10);

		GUILayout.BeginHorizontal();

		if(GUILayout.Button("Add Prefix",GUILayout.Width(100f)) && valuePrefix != ""){
			Debug.Log(Selection.activeObject.name);
			foreach(Object go in Selection.objects)
			{
				AssetDatabase.RenameAsset (go.name,valuePrefix + go.name)	;
				AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(go),valuePrefix + go.name);
			}
			valuePrefix = "";
			AssetDatabase.SaveAssets();
		}

		valuePrefix = EditorGUILayout.TextField("inputString",valuePrefix,GUILayout.MinWidth(50));

		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();

		if(GUILayout.Button("Add Suffix",GUILayout.Width(100f)) && valueSuffix != ""){
			foreach(Object go in Selection.objects)
			{
				AssetDatabase.RenameAsset (go.name,go.name + valueSuffix);
				AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(go),go.name + valueSuffix);
			}
			valueSuffix = "";
			AssetDatabase.SaveAssets();
		}

		valueSuffix = EditorGUILayout.TextField("inputString",valueSuffix,GUILayout.MinWidth(50));

		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();

		if(GUILayout.Button("Replace",GUILayout.Width(100f)) && valueReplaceString != ""){
			foreach(Object go in Selection.objects)
			{
				var name = go.name.ToCharArray();
				var replace = valueReplaceString.ToCharArray();
				List<char> newName = new List<char>(name);

				for(int i = valueRenameBegin;i< valueReplaceString.Length + valueRenameBegin; i++)
				{
					if(i < newName.Count)
						newName[i] = replace[i - valueRenameBegin];
					else
					{
						newName.Add(replace[i - valueRenameBegin]);
					}

				}
				AssetDatabase.RenameAsset (go.name,new string (newName.ToArray()));
				AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(go),new string (newName.ToArray()));
			}
			valueReplaceString = "";
			valueRenameBegin = 0;
			AssetDatabase.SaveAssets();
		}


		valueRenameBegin = EditorGUILayout.IntField("startIndex",valueRenameBegin, GUILayout.ExpandWidth(false));

		valueReplaceString = EditorGUILayout.TextField("inputString",valueReplaceString,GUILayout.MinWidth(50));

		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();

		if(GUILayout.Button("Delete",GUILayout.Width(100f)) && valueDeleteLenght != 0){
			foreach(Object go in Selection.objects)
			{
				var name = go.name.ToCharArray();
				List<char> newName = new List<char>();

				for(int i = 0;i< name.Length; i++)
				{
					if(i < valueDeleteBegin || i >= valueDeleteBegin + valueDeleteLenght)
						newName.Add(name[i]);
				}
				AssetDatabase.RenameAsset (go.name,new string (newName.ToArray()));
				AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(go),new string (newName.ToArray()));
			}

			valueDeleteBegin = 0;
			valueDeleteLenght = 0;
			AssetDatabase.SaveAssets();
		}

		valueDeleteBegin = EditorGUILayout.IntField("startIndex",valueDeleteBegin);
		valueDeleteLenght = EditorGUILayout.IntField("lenght",valueDeleteLenght);

		GUILayout.EndHorizontal();
	}
}
